output "kubeconfig" {
  value = "${data.external.kubeconfig.result["kubeconfig"]}"
}

output "depended_on" {
  value = "${null_resource.dependency_setter.id}"
}
