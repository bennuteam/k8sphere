resource "kubernetes_namespace" "name" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.tiller_enabled && var.tiller_namespace != "kube-system" ? 1 : 0}"

  metadata {
    name = "${var.tiller_namespace}"
  }
}

resource "kubernetes_service_account" "tiller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.tiller_enabled ? 1 : 0}"

  metadata {
    name      = "${var.tiller_service_account}"
    namespace = "${var.tiller_namespace}"
  }
}

resource "kubernetes_cluster_role_binding" "tiller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.tiller_enabled ? 1 : 0}"

  metadata {
    name = "${var.tiller_service_account}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.tiller.metadata.0.name}"
    namespace = "${kubernetes_service_account.tiller.metadata.0.namespace}"

    # See https://github.com/terraform-providers/terraform-provider-kubernetes/issues/204
    api_group = ""
  }
}

# See https://github.com/helm/helm/blob/master/cmd/helm/installer/install.go#L199
# See https://github.com/terraform-providers/terraform-provider-kubernetes/issues/38#issuecomment-318581203
resource "kubernetes_deployment" "tiller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.tiller_enabled ? 1 : 0}"

  metadata {
    name      = "tiller-deploy"
    namespace = "${kubernetes_service_account.tiller.metadata.0.namespace}"

    labels {
      app  = "helm"
      name = "tiller"
    }
  }

  spec {
    selector {
      match_labels {
        app  = "helm"
        name = "tiller"
      }
    }

    template {
      metadata {
        labels {
          app  = "helm"
          name = "tiller"
        }
      }

      spec {
        service_account_name = "${kubernetes_service_account.tiller.metadata.0.name}"

        container {
          name              = "tiller"
          image             = "gcr.io/kubernetes-helm/tiller:v2.12.3"
          image_pull_policy = "IfNotPresent"

          port {
            name           = "tiller"
            container_port = 44134
          }

          port {
            name           = "http"
            container_port = 44135
          }

          env {
            name  = "TILLER_NAMESPACE"
            value = "${kubernetes_service_account.tiller.metadata.0.namespace}"
          }

          env {
            name  = "TILLER_HISTORY_MAX"
            value = "200"
          }

          liveness_probe {
            http_get {
              path = "/liveness"
              port = 44135
            }

            initial_delay_seconds = 1
            timeout_seconds       = 1
          }

          readiness_probe {
            http_get {
              path = "/readiness"
              port = 44135
            }

            initial_delay_seconds = 1
            timeout_seconds       = 1
          }

          # See https://github.com/terraform-providers/terraform-provider-kubernetes/issues/38#issuecomment-318581203
          volume_mount {
            mount_path = "/var/run/secrets/kubernetes.io/serviceaccount"
            name       = "${kubernetes_service_account.tiller.default_secret_name}"
            read_only  = true
          }
        }

        # See https://github.com/terraform-providers/terraform-provider-kubernetes/issues/38#issuecomment-318581203
        volume {
          name = "${kubernetes_service_account.tiller.default_secret_name}"

          secret {
            secret_name = "${kubernetes_service_account.tiller.default_secret_name}"
          }
        }
      }
    }
  }
}
