resource "null_resource" "metrics-server" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.metrics_server_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/metrics_server.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl apply -f ${path.module}/templates/metrics_server.yml"
    on_failure = "continue"
  }
}
