variable "dependencies" {
  type = "list"
}

variable "config_path" {}
variable "dashboard_enabled" {}
variable "ingress_enabled" {}
variable "ingress_controller" {}
variable "ingress_controller_replicas" {}
variable "acme_enabled" {}
variable "acme_email" {}
variable "metallb_enabled" {}
variable "metallb_ip_range" {}
variable "metrics_server_enabled" {}
variable "rook_enabled" {}
variable "tiller_enabled" {}
variable "tiller_namespace" {}
variable "tiller_service_account" {}
