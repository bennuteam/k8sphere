resource "null_resource" "kubernetes-dashboard" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.dashboard_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/dashboard.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${path.module}/templates/dashboard.yml --force --grace-period 0"
    on_failure = "continue"
  }
}

data "template_file" "kubernetes-dashboard-traefik" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" && var.dashboard_enabled ? 1 : 0}"
  template   = "${file("${path.module}/templates/dashboard_ingress_rule_traefik.tpl")}"

  vars {
    namespace = "kube-system"
  }
}

resource "local_file" "kubernetes-dashboard-traefik" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" && var.dashboard_enabled ? 1 : 0}"
  content    = "${data.template_file.kubernetes-dashboard-traefik.0.rendered}"
  filename   = "${path.module}/templates/dashboard_ingress_rule_traefik.yml"
}

resource "null_resource" "kubernetes-dashboard-traefik" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" && var.dashboard_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.kubernetes-dashboard-traefik.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.kubernetes-dashboard-traefik.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}

data "template_file" "kubernetes-dashboard-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_controller == "nginx" && var.dashboard_enabled ? 1 : 0}"
  template   = "${file("${path.module}/templates/dashboard_ingress_rule_nginx.tpl")}"

  vars {
    namespace = "kube-system"
  }
}

resource "local_file" "kubernetes-dashboard-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_controller == "nginx" && var.dashboard_enabled ? 1 : 0}"
  content    = "${data.template_file.kubernetes-dashboard-nginx.0.rendered}"
  filename   = "${path.module}/templates/dashboard_ingress_rule_nginx.yml"
}

resource "null_resource" "kubernetes-dashboard-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_controller == "nginx" && var.dashboard_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.kubernetes-dashboard-nginx.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.kubernetes-dashboard-nginx.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}
