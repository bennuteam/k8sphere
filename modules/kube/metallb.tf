resource "null_resource" "metallb-system" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.metallb_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/metallb.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${path.module}/templates/metallb.yml --force --grace-period 0"
    on_failure = "continue"
  }
}

resource "kubernetes_config_map" "metallb-config" {
  depends_on = [
    "null_resource.dependency_getter",
    "null_resource.metallb-system",
  ]

  count = "${var.metallb_enabled ? 1 : 0}"

  metadata {
    name      = "config"
    namespace = "metallb-system"

    labels {
      app = "metallb"
    }
  }

  data {
    config = <<EOF
address-pools:
- name: default
  protocol: layer2
  addresses:
  - ${var.metallb_ip_range}
EOF
  }
}
