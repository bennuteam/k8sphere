data "template_file" "ingress-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" ? 1: 0}"
  template   = "${file("${path.module}/templates/nginx.tpl")}"

  vars {
    ingress_controller_replicas = "${var.ingress_controller_replicas}"
  }
}

resource "local_file" "ingress-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" ? 1: 0}"
  content    = "${data.template_file.ingress-nginx.0.rendered}"
  filename   = "${path.module}/templates/nginx.yml"
}

resource "null_resource" "ingress-nginx" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" ? 1: 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.ingress-nginx.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.ingress-nginx.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}

data "template_file" "ingress-nginx-node-port" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && !var.metallb_enabled ? 1 : 0}"
  template   = "${file("${path.module}/templates/nginx_svc.tpl")}"

  vars {
    namespace = "ingress-nginx"
    type      = "NodePort"
  }
}

resource "local_file" "ingress-nginx-node-port" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && !var.metallb_enabled ? 1 : 0}"
  content    = "${data.template_file.ingress-nginx-node-port.0.rendered}"
  filename   = "${path.module}/templates/nginx_svc.yml"
}

resource "null_resource" "ingress-nginx-node-port" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && !var.metallb_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.ingress-nginx-node-port.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.ingress-nginx-node-port.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}

data "template_file" "ingress-nginx-load-balancer" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && var.metallb_enabled ? 1 : 0}"
  template   = "${file("${path.module}/templates/nginx_svc.tpl")}"

  vars {
    namespace = "ingress-nginx"
    type      = "LoadBalancer"
  }
}

resource "local_file" "ingress-nginx-load-balancer" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && var.metallb_enabled ? 1 : 0}"
  content    = "${data.template_file.ingress-nginx-load-balancer.0.rendered}"
  filename   = "${path.module}/templates/nginx_svc.yml"
}

resource "null_resource" "ingress-nginx-load-balancer" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "nginx" && var.metallb_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.ingress-nginx-load-balancer.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.ingress-nginx-load-balancer.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}
