resource "kubernetes_service_account" "traefik-ingress-controller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  metadata {
    name      = "traefik-ingress-controller"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role" "traefik-ingress-controller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  metadata {
    name = "traefik-ingress-controller"
  }

  rule {
    api_groups = [""]

    resources = [
      "services",
      "endpoints",
      "secrets",
    ]

    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses"]

    verbs = [
      "get",
      "list",
      "watch",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "traefik-ingress-controller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  metadata {
    name = "traefik-ingress-controller"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${kubernetes_cluster_role.traefik-ingress-controller.metadata.0.name}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.name}"
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"
  }
}

resource "kubernetes_config_map" "traefik-cfg" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  metadata {
    name      = "traefik-cfg"
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"

    labels {
      app = "traefik"
    }
  }

  data {
    traefik.toml = <<EOF
logLevel = "DEBUG"
debug = true
checkNewVersion = true
ProvidersThrottleDuration = "5s"
MaxIdleConnsPerHost = 200
InsecureSkipVerify = true
defaultEntryPoints = ["http", "https"]
[lifeCycle]
requestAcceptGraceTimeout = "10s"
graceTimeOut = "10s"
[traefikLog]
filePath = "log/traefik.log"
format = "json"
[accessLog]
filePath = "log/access.log"
format = "json"
[retry]
attempts = 3
[healthcheck]
interval = "30s"
[entryPoints]
  [entryPoints.http]
  address = ":80"
  compress = true
  [entryPoints.https]
  address = ":443"
    [entryPoints.https.tls]
  [entryPoints.api]
    address=":8080"
[api]
  entryPoint = "api"
  dashboard = true
  debug = true
[api.statistics]
  RecentErrors = 10
[metrics]
  [metrics.statistics]
    recentErrors = 10
  [metrics.prometheus]
    entryPoint = "api"
    buckets = [0.1,0.3,1.2,5.0]
[rest]
  entryPoint = "api"
[ping]
  entryPoint = "api"
[kubernetes]
EOF
  }
}

# resource "kubernetes_config_map" "traefik-cfg" {
# depends_on = ["null_resource.dependency_getter"]
#   count = "${var.ingress_enabled && var.ingress_controller == "traefik" && var.acme_enabled ? 1 : 0}"

#   metadata {
#     name      = "traefik-cfg"
#     namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"

#     labels {
#       app = "traefik"
#     }
#   }

#   data {
#     traefik.toml = <<EOF
# logLevel = "DEBUG"
# debug = true
# checkNewVersion = true
# ProvidersThrottleDuration = "5s"
# MaxIdleConnsPerHost = 200
# InsecureSkipVerify = true
# defaultEntryPoints = ["http", "https"]
# [lifeCycle]
# requestAcceptGraceTimeout = "10s"
# graceTimeOut = "10s"
# [traefikLog]
# filePath = "log/traefik.log"
# format = "json"
# [accessLog]
# filePath = "log/access.log"
# format = "json"
# [retry]
# attempts = 3
# [healthcheck]
# interval = "30s"
# [entryPoints]
#   [entryPoints.http]
#   address = ":80"
#   compress = true
#   [entryPoints.https]
#   address = ":443"
#     [entryPoints.https.tls]
#   [entryPoints.api]
#     address=":8080"
# [api]
#   entryPoint = "api"
#   dashboard = true
#   debug = true
# [api.statistics]
#   RecentErrors = 10
# [metrics]
#   [metrics.statistics]
#     recentErrors = 10
#   [metrics.prometheus]
#     entryPoint = "api"
#     buckets = [0.1,0.3,1.2,5.0]
# [rest]
#   entryPoint = "api"
# [ping]
#   entryPoint = "api"
# [acme]
# email = "${var.acme_email}"
# storage = "acme/acme.json"
# entryPoint = "https"
# OnHostRule = true
# acmeLogging = true
#   [acme.httpChallenge]
#   entryPoint = "http"
# [kubernetes]
# EOF
#   }
# }

resource "kubernetes_deployment" "traefik-ingress-controller" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  metadata {
    name      = "traefik-ingress-controller"
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"

    labels {
      k8s-app = "traefik-ingress-lb"
    }
  }

  spec {
    replicas = "${var.ingress_controller_replicas}"

    selector {
      match_labels {
        k8s-app = "traefik-ingress-lb"
      }
    }

    template {
      metadata {
        labels {
          k8s-app = "traefik-ingress-lb"
          name    = "traefik-ingress-lb"
        }
      }

      spec {
        service_account_name             = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.name}"
        termination_grace_period_seconds = 60

        container {
          image = "traefik:alpine"
          name  = "traefik-ingress-lb"

          args = [
            "--configfile=/etc/traefik/traefik.toml",
          ]

          port {
            name           = "http"
            container_port = 80
          }

          port {
            name           = "https"
            container_port = 443
          }

          port {
            name           = "dashboard"
            container_port = 8080
          }

          volume_mount {
            name       = "${kubernetes_config_map.traefik-cfg.metadata.0.name}"
            mount_path = "/etc/traefik"
          }
        }

        volume {
          name = "${kubernetes_config_map.traefik-cfg.metadata.0.name}"

          config_map {
            name = "${kubernetes_config_map.traefik-cfg.metadata.0.name}"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "traefik-ingress-node-port" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" && !var.metallb_enabled ? 1 : 0}"

  metadata {
    name      = "traefik-ingress"
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"
  }

  spec {
    selector {
      k8s-app = "traefik-ingress-lb"
    }

    port {
      port        = 80
      target_port = 80
      name        = "http"
    }

    port {
      port        = 443
      target_port = 443
      name        = "https"
    }

    port {
      port        = 8080
      target_port = 8080
      name        = "dashboard"
    }

    type = "NodePort"
  }
}

resource "kubernetes_service" "traefik-ingress-load-balancer" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" && var.metallb_enabled ? 1 : 0}"

  metadata {
    name      = "traefik-ingress"
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"
  }

  spec {
    selector {
      k8s-app = "traefik-ingress-lb"
    }

    port {
      port        = 80
      target_port = 80
      name        = "http"
    }

    port {
      port        = 443
      target_port = 443
      name        = "https"
    }

    port {
      port        = 8080
      target_port = 8080
      name        = "dashboard"
    }

    type = "LoadBalancer"
  }
}

data "template_file" "traefik-ingress" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"
  template   = "${file("${path.module}/templates/traefik_ingress_rule.tpl")}"

  vars {
    namespace = "${kubernetes_service_account.traefik-ingress-controller.metadata.0.namespace}"
  }
}

resource "local_file" "traefik-ingress" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"
  content    = "${data.template_file.traefik-ingress.0.rendered}"
  filename   = "${path.module}/templates/traefik_ingress_rule.yml"
}

resource "null_resource" "traefik-ingress" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.ingress_enabled && var.ingress_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${local_file.traefik-ingress.filename}"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${local_file.traefik-ingress.filename} --force --grace-period 0"
    on_failure = "continue"
  }
}
