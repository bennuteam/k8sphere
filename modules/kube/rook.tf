# Using this flow since rook uses CRDs for deploying

resource "null_resource" "rook" {
  depends_on = ["null_resource.dependency_getter"]
  count      = "${var.rook_enabled ? 1 :0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = <<EOT
      kubectl apply -f ${path.module}/templates/rook_operator.yml
      sleep 10
      kubectl apply -f ${path.module}/templates/rook_cluster.yml
    EOT
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = <<EOT
      kubectl delete -f ${path.module}/templates/rook_cluster.yml --force --grace-period 0
      kubectl delete -f ${path.module}/templates/rook_operator.yml --force --grace-period 0
    EOT

    on_failure = "continue"
  }
}

resource "null_resource" "rook-traefik" {
  depends_on = [
    "null_resource.rook",
    "null_resource.dependency_getter",
  ]

  count = "${var.rook_enabled && var.ingress_controller == "traefik" ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/rook_ing_traefik.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${path.module}/templates/rook_ing_traefik.yml --force --grace-period 0"
    on_failure = "continue"
  }
}

resource "null_resource" "rook-nginx" {
  depends_on = [
    "null_resource.rook",
    "null_resource.dependency_getter",
  ]

  count = "${var.rook_enabled && var.ingress_controller == "nginx" ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/rook_ing_nginx.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${path.module}/templates/rook_ing_nginx.yml --force --grace-period 0"
    on_failure = "continue"
  }
}

resource "null_resource" "ceph-storage-class" {
  depends_on = [
    "null_resource.rook",
    "null_resource.dependency_getter",
  ]

  count = "${var.rook_enabled ? 1 : 0}"

  provisioner "local-exec" {
    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command = "kubectl apply -f ${path.module}/templates/ceph-storage-class.yml"
  }

  provisioner "local-exec" {
    when = "destroy"

    environment {
      KUBECONFIG = "${var.config_path}"
    }

    command    = "kubectl delete -f ${path.module}/templates/ceph-storage-class.yml --force --grace-period 0"
    on_failure = "continue"
  }
}
